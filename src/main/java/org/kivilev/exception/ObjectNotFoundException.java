/*
 * Copyright (c) 2023-06
 * Author: Kivilev Denis <kivilev.d@gmail.com>
 */

package org.kivilev.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(code = NOT_FOUND, reason = "Object not found")
public class ObjectNotFoundException extends RuntimeException {
}
