package org.kivilev.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "amazon.s3")
@Getter
@RequiredArgsConstructor
public class AmazonS3Config {
    private final String bucketName;
    private final String url;
    private final String accessKey;
    private final String secretKey;
}
