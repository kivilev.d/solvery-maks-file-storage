package org.kivilev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.ZonedDateTime;

@Component
@RequiredArgsConstructor
public class TimeUtils {
    private final Clock clock;

    public ZonedDateTime now() {
        return ZonedDateTime.now(clock);
    }
}
