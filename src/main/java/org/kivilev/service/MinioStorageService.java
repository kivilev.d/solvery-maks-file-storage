package org.kivilev.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.RequiredArgsConstructor;
import org.kivilev.config.AmazonS3Config;
import org.kivilev.model.File;
import org.kivilev.model.FileInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MinioStorageService {
    private static final String USER_METADATA_ORIGINAL_FILENAME_PARAM = "Original-Filename";
    private final AmazonS3 amazonS3Client;
    private final FileUtils fileUtils;
    private final AmazonS3Config amazonS3Config;

    public List<FileInfo> addFilesToStorage(List<MultipartFile> multipartFiles) {
        return multipartFiles.stream()
                .map(this::uploadMultipartFile)
                .collect(Collectors.toList());
    }

    public List<FileInfo> getFileInfos() {
        var response = amazonS3Client.listObjects(amazonS3Config.getBucketName());
        return response.getObjectSummaries().stream()
                .map(s3ObjectSummary -> {
                    var fileKey = s3ObjectSummary.getKey();
                    var metadata = amazonS3Client.getObjectMetadata(amazonS3Config.getBucketName(), fileKey);
                    var originalFilename = metadata.getUserMetadata().get(USER_METADATA_ORIGINAL_FILENAME_PARAM);
                    return new FileInfo(UUID.fromString(fileKey), originalFilename, metadata.getContentType());
                }).toList();
    }

    public File getFile(String fileKey) {
        var s3File = amazonS3Client.getObject(amazonS3Config.getBucketName(), fileKey);
        var metadata = s3File.getObjectMetadata();
        String originalFilename = metadata.getUserMetadata().get(USER_METADATA_ORIGINAL_FILENAME_PARAM);
        String contentType = metadata.getContentType();

        try (var contentStream = amazonS3Client.getObject(amazonS3Config.getBucketName(), s3File.getKey()).getObjectContent()) {
            var tmpFilename = String.format("%s-%s", s3File.getKey(), UUID.randomUUID());
            Path destinationFile = fileUtils.toFile(tmpFilename, contentStream);
            return new File(new FileInfo(UUID.fromString(fileKey), originalFilename, contentType), fileUtils.fromFile(destinationFile));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private FileInfo uploadMultipartFile(MultipartFile multipartFile) {
        String s3FileKey = fileUtils.generateFileName();
        String originalFilename = multipartFile.getOriginalFilename();
        String contentType = multipartFile.getContentType();

        try (var stream = multipartFile.getInputStream()) {
            uploadFile(stream, s3FileKey, originalFilename, contentType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return new FileInfo(UUID.fromString(s3FileKey), originalFilename, contentType);
    }

    private void uploadFile(InputStream inputStream, String s3FileKey, String originalFilename, String contentType) {
        var metadata = new ObjectMetadata();
        metadata.addUserMetadata(USER_METADATA_ORIGINAL_FILENAME_PARAM, originalFilename);
        metadata.setContentType(contentType);
        amazonS3Client.putObject(new PutObjectRequest(amazonS3Config.getBucketName(), s3FileKey, inputStream, metadata)
                .withCannedAcl(CannedAccessControlList.PublicRead)
        );
    }
}
