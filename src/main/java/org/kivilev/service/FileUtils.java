package org.kivilev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class FileUtils {

    public Path toFile(String fileKey, InputStream inputStream) {
        try {
            Path destinationFile = Files.createTempFile(Objects.requireNonNull(fileKey), ".file");
            Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
            return destinationFile;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Resource fromFile(Path file) {
        return new FileSystemResource(file);
    }

    public String generateFileName() {
        return UUID.randomUUID().toString();
    }
}