package org.kivilev.controller;

import lombok.RequiredArgsConstructor;
import org.kivilev.controller.mapper.FileMapperDto;
import org.kivilev.controller.model.FileInfoResponseDto;
import org.kivilev.service.MinioStorageService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
public class FileController {

    private final MinioStorageService storageService;
    private final FileMapperDto fileMapperDto;

    @GetMapping("/api/files/")
    public List<FileInfoResponseDto> getFileInfos() {
        return fileMapperDto.toDto(storageService.getFileInfos());
    }

    @GetMapping(value = "/api/files/{id}/")
    public ResponseEntity<Resource> getFile(@PathVariable("id") UUID fileKey) {
        var file = storageService.getFile(fileKey.toString());
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.parseMediaType(file.getFileInfo().getContentType()));
        return new ResponseEntity<>(file.getResource(), httpHeaders, HttpStatus.OK);
    }

    @PostMapping(value = "/api/files/", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<FileInfoResponseDto> uploadFiles(@RequestParam("file") List<MultipartFile> files) {
        return fileMapperDto.toDto(storageService.addFilesToStorage(files));
    }
}
