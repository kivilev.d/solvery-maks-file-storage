package org.kivilev.controller.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@RequiredArgsConstructor
@Getter
public class FileInfoResponseDto {
    private final UUID fileId;
    private final String filename;
    private final String contentType;
}
