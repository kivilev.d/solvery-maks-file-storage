package org.kivilev.controller.mapper;

import org.kivilev.controller.model.FileInfoResponseDto;
import org.kivilev.model.FileInfo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FileMapperDto {
    public List<FileInfoResponseDto> toDto(List<FileInfo> fileInfos) {
        return fileInfos.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    private FileInfoResponseDto toDto(FileInfo fileInfo) {
        return new FileInfoResponseDto(fileInfo.getFileId(), fileInfo.getOriginalFileName(), fileInfo.getContentType());
    }
}
