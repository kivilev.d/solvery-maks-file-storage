package org.kivilev.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;

@RequiredArgsConstructor
@Getter
public class File {
    private final FileInfo fileInfo;
    private final Resource resource;
}
