package org.kivilev.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@RequiredArgsConstructor
@Getter
public class FileInfo {
    private final UUID fileId;
    private final String originalFileName;
    private final String contentType;
}
